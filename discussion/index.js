// index.js

console.log("Hello World!")

// ARRAYS

/*

-Arrays are used to store multiple related data/values in a single variable. It is created/declared using [] brackets also known as "Array Literals".
- Arrays make it easy to manage, manipulate a set of data. Arrays have different methods/functions that allow us to manage our array.
-Methods are functions associated with an object.
- Arrays are actually a special type of object.
- Arrays as a best practice contain values of the same data type. However, since there are no problems creating arrays like this (arraySample), you may encounter exceptions to this rule in the future and in fact in other JS libraries of framework.
- Array as a collection of data, has methods to manipulate and manage the array. Having values with diff data types might interfere or conflict with the methods of an array.
- Array as a collection of data, as a convention, its name is usually plural.
- We can also add the values of variables as elements in an array.
*/

let hobbies = ["Play video games","Read a book","Listen to music"];
console.log(typeof hobbies);

let grades = [75.4,98.5,90.12,91.50];
const planets = ["Mercury","Venus","Mars","Earth"];

let arraySample = ["Saitama","One Punch Man",25000,true];

/*

	Mini-Activity

	1.Create a variable which will store an array of at least 5 of your daily routine or task.
	2. Create a variable which will store at least 4 capital cities in the world.

*/

let routine = ["1. Brush teeth.","2. Take a bath.", "3. Go to class","4. Eat foodss","5. Do dailies"];
console.log(routine);

let capital = ["Manila","Kathmandu","Amsterdam","Berlin"];
console.log(capital);

//---
let tasks = ["Brush teeth","Eat Breakfast","Go to Bootcamp","Eat lunch","Play games"];
let capitalCities = [

	"Tokyo",
	"Ottawa",
	"Manila",
	"Hanoi"

];

console.log(tasks);
console.log(capitalCities);

// Each item in an array is called an element.

let username1 = "fighter_smith1";
let username2 = "georgeKyle5000";
let username3 = "white_night";

let guildMembers = [username1,username2,username3];

console.log(guildMembers);


// .length property - is a number type that tells the number of elements in an array 

console.log(capitalCities.length);
console.log(tasks.length);


// .length for deleting

tasks.length = tasks.length-1;
console.log(tasks.length);
console.log(tasks);

capitalCities.length--;
console.log(capitalCities);

// .length for adding

tasks.length = tasks.length+1;
console.log(tasks.length);
console.log(tasks);

capitalCities.length++;
console.log(capitalCities);

// delete with .length doesn't work with strings

fullName = "Randy Orton";
fullName.length = fullName.length-1;
console.log(fullName);


// ARRAY INDICES

// Syntax: arrayName[index]

// to access elements of an array that are ordered according to index, which are number types

console.log(capitalCities[0]);
console.log(capitalCities[1]);
console.log(capitalCities[2]);
console.log(capitalCities[3]);

let lakersLegends = ["Kobe","Shaq","Lebron","Magic","Kareem"];

console.log(lakersLegends[1]);
console.log(lakersLegends[3]);

// save an element in a variable

let currentLaker = lakersLegends[2];
console.log(currentLaker);

// update/reassign elements

lakersLegends[2] = "Pau Gasol";
console.log(lakersLegends);

/*

	Mini-Activity

	1. Update/reassign the last two items in the array with your own personal fave.

*/

let favoriteFoods = [
	
	"Tonkatsu",
	"Adobo",
	"Hamburger",
	"Sinigang",
	"Pizza"

];


favoriteFoods[3] = "Takoyaki";
favoriteFoods[4] = "Shawarma";
console.log(favoriteFoods);

// access last iem by adding .length property value minus 1 as index

console.log(favoriteFoods[favoriteFoods.length-1]);

let bullsLegends = ["Jordan","Pippen","Rodman","Rose","Scalabrine","Kukoc","LaVine","Longley","DeRozan"];
console.log(bullsLegends[bullsLegends.length-1]);


// adding items in an array

let newArr = [];
console.log(newArr);
console.log(newArr[0]);

newArr[0] = "Cloud Strife"; //added first element
console.log(newArr);

console.log(newArr[1]);
newArr[1] = "Aerith Gainsborough"; //added second element
console.log(newArr);

newArr[newArr.length-1] = "Tifa Lockhart"; //replaced second element
console.log(newArr);

newArr[newArr.length] = "Barrett Wallace";
console.log(newArr);

// looping array - loop and iterate all elements in the array


for(let index = 0; index < newArr.length; index++){
	console.log(newArr[index]);
}

let numArr = [5,12,30,46,40];

for (let index = 0; index < numArr.length; index++){

	if(numArr[index] % 5 === 0){
		console.log(numArr[index] + " is divisible by 5");
	} else {
		console.log(numArr[index] + " is not divisible by 5");
	}

}


// multidimensional arrays - arrays within an array and access by arrayName[row][column]

let chessBoard = [
    ['a1', 'b1', 'c1', 'd1', 'e1', 'f1', 'g1', 'h1'],
    ['a2', 'b2', 'c2', 'd2', 'e2', 'f2', 'g2', 'h2'],
    ['a3', 'b3', 'c3', 'd3', 'e3', 'f3', 'g3', 'h3'],
    ['a4', 'b4', 'c4', 'd4', 'e4', 'f4', 'g4', 'h4'],
    ['a5', 'b5', 'c5', 'd5', 'e5', 'f5', 'g5', 'h5'],
    ['a6', 'b6', 'c6', 'd6', 'e6', 'f6', 'g6', 'h6'],
    ['a7', 'b7', 'c7', 'd7', 'e7', 'f7', 'g7', 'h7'],
    ['a8', 'b8', 'c8', 'd8', 'e8', 'f8', 'g8', 'h8']
];

console.log(chessBoard);
console.log(chessBoard[1][5]);  //result: f2

/*

	Mini-Activity

	1. Log 'a8' in the console from our chessBoard multi-dimensional array.

*/

console.log(chessBoard[7][0]);
console.log(chessBoard[5][7]);

